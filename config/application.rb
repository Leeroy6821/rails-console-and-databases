# frozen_string_literal: true

require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_view/railtie'
require 'sprockets/railtie'
# require 'action_cable/engine'
# require 'action_mailer/railtie'
# require 'active_storage/engine'
# require 'active_job/railtie'
# require "rails/test_unit/railtie"

Bundler.require(*Rails.groups)

module Homework4
  class Application < Rails::Application
    config.load_defaults 5.2

    config.generators.system_tests = nil
  end
end
